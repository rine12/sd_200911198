package com.ex.ood;

/* Ch3 Assignment 
Comparator
Kim Dong Hyuk 200911198 */

import java.lang.*;

public interface Comparator {
    public int compare(Object obj1,Object obj2);
}
