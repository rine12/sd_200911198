package com.ex.ood;
public class Ch7GarageDoorOpenCommand implements Ch7Command{
	Ch7GarageDoor gDoor;
	public Ch7GarageDoorOpenCommand(Ch7GarageDoor garageDoor) {
		this.gDoor=garageDoor;
	}
	public void execute() {
		gDoor.up();
	}
}
