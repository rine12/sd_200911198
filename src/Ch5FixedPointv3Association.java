package com.ex.ood;
import java.awt.Point;
public class Ch5FixedPointv3Association {
	private Point pt;
	public Ch5FixedPointv3Association(int x, int y) {
	//this.pt.x=x;
	//this.pt.y=y;
	this.pt=new Point(x,y);
	}
	public double getX() {return pt.getX();}
	public double getY() {return pt.getY();}
	public Point getLocation() {
		//return pt;
		//no good because pt is changeable(p.154)
		return pt.getLocation();
	}
}
