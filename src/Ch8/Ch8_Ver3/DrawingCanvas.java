package com.ex.ood.drawer3;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class DrawingCanvas extends JPanel {
   private Drawing drawing;
   public DrawingCanvas() {
	this.drawing = new Drawing();
	//more..
   }
   public void paintComponent(Graphics g) {
	super.paintComponent(g);
	for(Figure figure : drawing) {
	   figure.draw(g);
	}
   }

   public void addFigure(Figure newFigure) {
	drawing.addFigure(newFigure);
	repaint();
   }
}
