package com.ex.ood.drawer3;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.Iterator;

public class Drawing implements Iterable<Figure> {
   private List<Figure> figures; //collection of Figures
   public Drawing() {
	figures = new ArrayList<Figure>();
   }
   public void addFigure(Figure newFigure) {
	figures.add(newFigure);
   }
   public Iterator<Figure> iterator() {
	return figures.iterator();
   }
}
