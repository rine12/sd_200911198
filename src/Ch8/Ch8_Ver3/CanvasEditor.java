package com.ex.ood.drawer3;

import javax.swing.*;
import java.awt.event.*;

public class CanvasEditor implements MouseListener {
   
   private Figure currentFigure;
   public CanvasEditor(Figure figure) {
	this.currentFigure = figure;
   }
   public void setCurrentFigure(Figure newFigure) {
	currentFigure = newFigure;
   }  
   public Object clone(){
	try{
	   return super.clone();
	} catch (CloneNotSupportedException e) {
	  assert false; //this code block should never execute
	  return null;
	}
   }
   public void mouseClicked(MouseEvent e){ 
   Figure newFigure = (Figure) currentFigure.clone();
   newFigure.setCenter(e.getX(), e.getY());
   ((DrawingCanvas) e.getSource()).addFigure(newFigure);
   }
   public void mousePressed(MouseEvent e){}
   public void mouseReleased(MouseEvent e){}
   public void mouseEntered(MouseEvent e){}
   public void mouseExited(MouseEvent e){}
}

