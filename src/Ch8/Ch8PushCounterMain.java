package com.ex.ood;

import javax.swing.JFrame;

public class Ch8PushCounterMain{
    public static void main(String[] args){
        JFrame frame=new JFrame("Push Counter");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Ch8PushCounterPanel panel=new Ch8PushCounterPanel();
        frame.getContentPane().add(panel);
        frame.pack();
        frame.setVisible(true);  
    }
}
