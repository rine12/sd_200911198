package com.ex.ood.ch8.drawer1;

import javax.swing.*;
import java.awt.event.*;


public class Ch8CanvasEditor implements ActionListener, MouseListener{
    private JButton currentButton;
    public void actionPerformed(ActionEvent ae){
        currentButton=(JButton) ae.getSource();
    }
    //public CanvasEditor(JButton in
    public void mouseClicked(MouseEvent e){
        int x=e.getX();
        int y=e.getY();
        JPanel canvas=(JPanel) e.getSource();
        if(currentButton.getText().equals("Ellipse"))
            canvas.getGraphics().drawOval(x-30,y-20,60,40);
        else if(currentButton.getText().equals("Rect"))
            canvas.getGraphics().drawRect(x-30,y-20,60,40);
        else//if(currentButton.getText().equals("Square"))
            canvas.getGraphics().drawRect(x-25,y-25,50,50);

    }
    public void mousePressed(MouseEvent e){}
    public void mouseReleased(MouseEvent e){}
    public void mouseEntered(MouseEvent e){}
    public void mouseExited(MouseEvent e){}

}
