package com.ex.ood.ch8.drawer1;

import javax.swing.*;
import java.awt.*;
/**
 *@author Dale Skrien
 *@version 1.0
 */

public class Ch8DrawingFrame1 extends JFrame{
    public Ch8DrawingFrame1(){
        super("Drawing Application");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        
        JComponent drawingCanvas =createDrawingCanvas();
        add(drawingCanvas, BorderLayout.CENTER);
        
        JToolBar toolbar=createToolbar(drawingCanvas);
        add(toolbar, BorderLayout.NORTH);
    }
    

    /**
     *create the canvas

     */

    private JComponent createDrawingCanvas(){
        JComponent drawingCanvas=new JPanel();
        drawingCanvas.setPreferredSize(new Dimension(400,300));
        drawingCanvas.setBackground(Color.white);
        drawingCanvas.setBorder(BorderFactory.createEtchedBorder());
        return drawingCanvas;
    }

    /*

     */
    private JToolBar createToolbar(JComponent canvas){
        JToolBar toolbar=new JToolBar();
        JButton ellipseButton =new JButton("Ellipse");
        toolbar.add(ellipseButton);
        JButton squareButton =new JButton("Square");
        toolbar.add(squareButton);
        JButton rectButton=new JButton("Rect");
        toolbar.add(rectButton);
       

        //add the CanvasEditor listener to the canvas and to the button
        //with the ellipseButton initially selected
        Ch8CanvasEditor canvasEditor=new Ch8CanvasEditor();
        ellipseButton.addActionListener(canvasEditor);
        squareButton.addActionListener(canvasEditor);
        rectButton.addActionListener(canvasEditor);
        canvas.addMouseListener(canvasEditor);


     

         return toolbar;
    }

   
} 
