package com.ex.ood;

import javax.swing.*;
import java.awt.*;


public class Ch8SimpleDrawMain extends JFrame{
    public Ch8SimpleDrawMain(){
        setSize(200,200);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
    //public static void main(String[] args){
    //    Ch8SimpleDrawMain drawFrame=new Ch8SimpleDrawMain();
    //    drawFrame.setVisible(true);
    //}
    public void paint(Graphics g){
        g.drawRect(10,20,30,40);
        g.drawOval(50,60,70,80);
    }
}
