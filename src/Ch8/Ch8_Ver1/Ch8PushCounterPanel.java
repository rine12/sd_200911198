package com.ex.ood;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Ch8PushCounterPanel extends JPanel{
    private int count;
    private JButton push;
    private JLabel label;

    public Ch8PushCounterPanel(){
        count=0;
        push=new JButton("push me!");
        push.addActionListener(new ButtonListener());
        label=new JLabel("Pushes: "+count);
        add(push);
        add(label);
        setBackground(Color.cyan);
        setPreferredSize(new Dimension(300,40));
    }
    private class ButtonListener implements ActionListener{
        public void actionPerformed(ActionEvent event){
            count++;
            label.setText("Pushes: "+count);
        }
    }
}        

 
