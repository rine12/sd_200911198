package com.ex.ood.drawer2;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class DrawingFrame extends JFrame{
   
    public DrawingFrame(){
        super("Drawing Application");
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        JComponent drawingCanvas = createDrawingCanvas();
        add(drawingCanvas, BorderLayout.CENTER);

        JToolBar toolbar = createToolbar(drawingCanvas);
        add(toolbar, BorderLayout.NORTH);
    }

    private JComponent createDrawingCanvas(){
        JComponent drawingCanvas = new JPanel();
        drawingCanvas.setPreferredSize(new Dimension(400, 300));
        drawingCanvas.setBackground(Color.white);
        drawingCanvas.setBorder(BorderFactory.createEtchedBorder());
        return drawingCanvas;
    }

    private JToolBar createToolbar(JComponent canvas){
        JToolBar toolbar = new JToolBar();
        //add the buttons to the toolbar
        JButton ellipseButton = new JButton("Ellipse");
        toolbar.add(ellipseButton);
        JButton squareButton = new JButton("Square");
        toolbar.add(squareButton);
        JButton rectButton = new JButton("Rect");
        toolbar.add(rectButton);

        //add the listeners to the buttons and canvas
        final CanvasEditor canvasEditor =
                new CanvasEditor(new Ellipse(0, 0, 60, 40));
        ellipseButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent ae){
                canvasEditor.setCurrentFigure(new Ellipse(0, 0, 60, 40));
            }
        });
        squareButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent ae)
            {
                canvasEditor.setCurrentFigure(new Square(0, 0, 50));
            }
        });
        rectButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent ae)
            {
                canvasEditor.setCurrentFigure(new Rect(0, 0, 60, 40));
            }
        });
        canvas.addMouseListener(canvasEditor);

        return toolbar;
    }
}
