package com.ex.ood.drawer2;

import java.awt.*;

public abstract class Figure {
    private int centerX, centerY;
    private int width;
    private int height;

    public Figure(int centerX,int centerY,int w,int h){
        this.centerX=centerX;
        this.centerY=centerY;
        this.width=w;
        this.height=h;
    }
    public int getWidth(){
        return width;
    }
    public int getHeight(){
        return height;
    }
    public void setCenter(int centerX,int centerY){
        this.centerX=centerX;
        this.centerY=centerY;
    }
    public int getCenterX(){
        return centerX;
    }
    public int getCenterY(){
        return centerY; 
    }
    public abstract void draw(Graphics g);

}
 
