package com.ex.ood.ch8;

import javax.swing.*;
import java.awt.*;
/**
 *@author Dale Skrien
 *@version 1.0
 */

public class Ch8DrawingFrame extends JFrame{
    public Ch8DrawingFrame(){
        super("Drawing Application");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        
        JComponent drawingCanvas =createDrawingCanvas();
        add(drawingCanvas, BorderLayout.CENTER);
        
        JToolBar toolbar=createToolbar();
        add(toolbar, BorderLayout.NORTH);
    }
    

    /**
     *create the canvas(the part of the window below the tool bar)
     *@return the new JComponent that is the canvas
     */

    private JComponent createDrawingCanvas(){
        JComponent drawingCanvas=new JPanel();
        drawingCanvas.setPreferredSize(new Dimension(400,300));
        drawingCanvas.setBackground(Color.white);
        drawingCanvas.setBorder(BorderFactory.createEtchedBorder());
        return drawingCanvas;
    }

    /**
     *create the tool bar with the three buttons 
     *@return the new tool bar
     */
    private JToolBar createToolbar(){
        JToolBar toolbar=new JToolBar();
        JButton ellipseButton =new JButton("Ellipse");
        toolbar.add(ellipseButton);
        JButton squareButton =new JButton("Square");
        toolbar.add(squareButton);
        JButton rectButton=new JButton("Rect");
        toolbar.add(rectButton);
        return toolbar;
    }

   /**
    *creates and displays a DrawingFrame object
    *@param args
    */
    public static void main(String[] args){
        Ch8DrawingFrame drawFrame=new Ch8DrawingFrame();
        drawFrame.pack();
        drawFrame.setVisible(true);
    }
} 
