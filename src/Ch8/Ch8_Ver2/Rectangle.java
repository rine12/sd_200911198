package com.ex.ood.drawer2;

import java.awt.*;

public class Rectangle extends Figure{
    public Rectangle(int x, int y, int w, int h){
        super(x, y, w, h);
    }

    public void draw(Graphics g){
        int width = getWidth();
        int height = getHeight();
        g.drawRect(getCenterX() - width / 2, getCenterY() - height / 2,
                width, height);
    }
}
