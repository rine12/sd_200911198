package com.ex.ood.drawer2;

import javax.swing.*;
import java.awt.event.*;

public class CanvasEditor implements MouseListener {
   private Figure currentFigure;
   public CanvasEditor(Figure figure) {
	this.currentFigure = figure;
   }
   public void setCurrentFigure(Figure newFigure) {
	currentFigure = newFigure;
   }
   public void mouseClicked(MouseEvent e) {
	JPanel canvas = (JPanel) e.getSource();
	currentFigure.setCenter(e.getX(), e.getY());
	currentFigure.draw(canvas.getGraphics());
   }
   public void mousePressed(MouseEvent e){}
   public void mouseReleased(MouseEvent e){}
   public void mouseEntered(MouseEvent e){}
   public void mouseExited(MouseEvent e){}
}

