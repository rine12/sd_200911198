package com.ex.ood;

public class SoldOutState implements State {
   public void soldOut(){
      System.out.println("매진되었습니다.");
   }
   public void insertQuarter(){}
   public void ejectQuarter(){}
   public void turnCrank(){}
   public void dispense(){}
}
