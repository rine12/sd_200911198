package com.ex.ood;
public class Ch7PizzaStore{
	Ch7SimplePizzaFactory factory;
	public Ch7PizzaStore(Ch7SimplePizzaFactory factory){
		this.factory=factory;
	}
	public Ch7Pizza orderPizza(String type) {
		Ch7Pizza pizza;
		pizza=factory.createPizza(type);
		pizza.prepare();
		pizza.bake();
		pizza.cut();
		pizza.box();
		return pizza;
	}
}
