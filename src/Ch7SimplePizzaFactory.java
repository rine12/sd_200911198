package com.ex.ood;
public class Ch7SimplePizzaFactory {
	Ch7Pizza pizza;
	public Ch7Pizza createPizza(String type) {
		if(type.equals("cheese"))
			pizza=new Ch7CheesePizza();
		return pizza;
	}
}
