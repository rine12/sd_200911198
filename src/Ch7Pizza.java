package com.ex.ood;
import java.util.ArrayList;
public abstract class Ch7Pizza {
	String name;
	String dough;
	String sauce;
	ArrayList toppings=new ArrayList();
	void prepare() {
		System.out.print("Pizza preparing..");
	}
	void bake() {
		System.out.print("backing..");
	}
	void cut() {
		System.out.print("cutting..");
	}
	void box() {
		System.out.print("boxing..");
	}
	public String getName() {
		return this.name;
	}
}

