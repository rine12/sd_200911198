package com.ex.ood;
import java.lang.*;

/* Ch3 Assignment 
Comparator
Kim Dong Hyuk 200911198 */

public class IntegerComparator implements Comparator {
    public int compare(Object obj1,Object obj2) {
	return (Integer) obj1 - (Integer) obj2 ;
    }
}
