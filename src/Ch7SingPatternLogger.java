package com.ex.ood;

public class Ch7SingPatternLogger {
	public static Ch7SingPatternLogger uniqueInstance;

	//no one can instantiate this!
	private Ch7SingPatternLogger(){}

	//define as static -> Ch5SingPatternLogger.getInstance()
	public static Ch7SingPatternLogger getInstance(){
		if(uniqueInstance==null)
			uniqueInstance = new Ch7SingPatternLogger();
		return uniqueInstance;
	}
	public void readEntireLog() {
		System.out.println("Singleton log goes here");
	}
}
