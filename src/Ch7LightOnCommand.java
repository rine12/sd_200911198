package com.ex.ood;
public class Ch7LightOnCommand implements Ch7Command {
	Ch7Light light;
	public Ch7LightOnCommand(Ch7Light light) {
		this.light = light;
	}
	@Override
	public void execute() {
		// TODO Auto-generated method stub
		light.on();
	}
}
