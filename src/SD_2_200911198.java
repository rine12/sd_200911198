package com.ex.ood;

import com.ex.ood.*;
import java.awt.Rectangle;
import java.awt.Point;
import java.awt.Color;
import java.util.*;

public class SD_2_200911198{
public static void main(String[] args) {
	main();
}

private static void main() {

//1.Enhanced Rectangle

	System.out.println("----------Enhanced Rectangle----------");
	EnhancedRectangle rectangle = new EnhancedRectangle(10,20,100,200);

	System.out.println("\n\n-------- 초기값, 위치, 중앙 ------ --");
	System.out.println("초기값 : "+rectangle.toString());
	System.out.println("위치 : " + rectangle.getLocation());
	System.out.println("중앙 : " + rectangle.getCenter());
	
	System.out.println("\n\n위치설정(1,1)");
	rectangle.setLocation(1,1);
	System.out.println("위치 : " + rectangle.getLocation()+"\n");
	
	System.out.println("위치설정(10,10)");
	rectangle.setCenter(10,10);
	System.out.println("중앙 : " + rectangle.getCenter()+"\n");

	System.out.println("\n-- 결과값,  위치, 중앙 --");
	System.out.println("결과값: "+rectangle.toString());
        System.out.println("위치 : " + rectangle.getLocation());
        System.out.println("중앙 : " + rectangle.getCenter());


//2.Linked List VS Array List

System.out.println("\n\n-------Linked List vs. Array List------\n");

LinkedList<String> link = new LinkedList<String>();
ArrayList<String> array = new ArrayList<String>();

link.add("a_link");
link.add("b_link");
link.add("c_link");
link.add("d_link");

array.add("Z_array");
array.add("Y_array");
array.add("X_array");
array.add("W_array");

Iterator<String> itr2 = link.iterator();
System.out.println("iterator를 이용한 linkedlist 출력\n");

while(itr2.hasNext()){
System.out.println(itr2.next());
}

System.out.println("\niterator를 이용한 arraylist 출력 \n");

Iterator<String> itr = array.iterator();

while(itr.hasNext()){
System.out.println(itr.next());
}
System.out.println("\nLinkedlist의 크기 : " + link.size());
System.out.println("\nArraylist의 크기 : " + array.size());

array.clear();
link.clear();

System.out.println("\nClear 이후에 Linkedlist의 크기 : " + link.size());
System.out.println("\nClear 이후에 Arraylist의 크기 : " + array.size());

//3.Automobile Polymorphism

System.out.println("\n\n----------Automobile polymorphism----------\n");

int totalCapacity = 0;

Automobile[] fleet = new Automobile[3];
fleet[0] = new Sonata(Color.red);
fleet[1] = new Truck(Color.green);
fleet[2] = new Tico(Color.blue); 

//Version1
for(int i=0; i<fleet.length; i++)
{
	if(fleet[i] instanceof Sonata)
	totalCapacity += ((Sonata) fleet[i]).getCapacity();
	else if(fleet[i] instanceof Truck)
	totalCapacity += ((Truck) fleet[i]).getCapacity();
	else if(fleet[i] instanceof Tico)
	totalCapacity += ((Tico) fleet[i]).getCapacity();
	else
	totalCapacity += fleet[i].getCapacity();
}

System.out.println("자동차의 총 수용량 : " + totalCapacity+"\n");

//Version2
totalCapacity=0;

for(int i=0; i<fleet.length ; i++){
	totalCapacity +=fleet[i].getCapacity();
}
System.out.println("다형성을 이용한 자동차의 총 수용량 : " + totalCapacity+"\n");
}

//---------------------------------------------------------
public static class EnhancedRectangle extends Rectangle
{
	public EnhancedRectangle(int x, int y, int w, int h)
	{
	super(x,y,w,h);
	}
	public Point getCenter()
	{
	return new Point((int)getCenterX(), (int)getCenterY());
	}
	public void setCenter(int x, int y)
	{
		setLocation(x-(int)getWidth()/2, y - (int) getHeight()/2);
	}
}

public static class Automobile
{
int Capacity = 1;

	Color cl;

	public Automobile(Color color)
	{
	cl=color;
	}

	public int getCapacity()
	{
	return Capacity;
	}
}

public static class Sonata extends Automobile
{
	public Sonata (Color color)
	{
	super(color);
	Capacity=5;
	}

}

public static class Truck extends Automobile
{

public Truck (Color color)
{
	super(color);
	Capacity=12;
}
}

public static class Tico extends Automobile
{
	public Tico(Color color)
 	{
	super(color);
	Capacity=4;
	}
}
}

