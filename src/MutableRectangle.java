package com.ex.ood;
import com.ex.ood.Rectangle;

/* Ch3 Assignment 
Comparator
Kim Dong Hyuk 200911198 */

public class MutableRectangle extends Rectangle {
    public MutableRectangle (int x,int y, int wid, int hei)
    {
	super(x,y,wid,hei);
    }
    public void setSize(int wid,int hei) {
        super.setWidth(wid);
        super.setHeight(hei);
    }

}
