package com.ex.ood;

import com.ex.ood.*;
import java.awt.Point;
import java.util.*;


public final class Ch5FixedPointv1New {
	private int x;
	private int y;
	public Ch5FixedPointv1New(int x, int y) {
		this.x=x;
		this.y=y;
	}
	public double getX() {return x;}
	public double getY() {return y;}

public static void main(String[] args) {
	Ch5main();
}

private static void Ch5main() {
	System.out.println("--Ch5--FixedPoint \n\n");
	Ch5FixedPointv1New fp1 = new Ch5FixedPointv1New(10,20);
	
	System.out.println(fp1);

}

}
