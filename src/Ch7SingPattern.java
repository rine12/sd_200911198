package com.ex.ood;
import com.ex.ood.Ch7SingPatternLogger;
import com.ex.ood.Ch7SingPatternLoggerBefore;

public class  Ch7SingPattern {
	public static void main(String args[])
	{
		Ch7SingPatternLogger ch7 = new Ch7SingPatternLogger();
		ch7.readEntireLog();
		ch7.getInstance();
	Ch7SingPatternLoggerBefore CH7 = new Ch7SingPatternLoggerBefore();
		CH7.readEntireLog();
	}
}
	
